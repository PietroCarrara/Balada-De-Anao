/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author pietro
 */

// 
public class ForteAnao implements Serializable {
    
    public static final String DWARF_FILE = "anoes.dat",
                                PARTY_FILE = "festas.dat";
    
    public static List<FestaDeAnao> pegaAsFesta () {
         
        List<FestaDeAnao> f;
        
        try {
            InputStream fos = new FileInputStream(PARTY_FILE);
            ObjectInputStream ois = new ObjectInputStream(fos);
            f = (List<FestaDeAnao>) ois.readObject();
            fos.close();
            ois.close();
            return f;
        } catch (Exception e) {
            return new ArrayList<FestaDeAnao>();
        }
    }
    
    public static List<Anao> pegaOsAnao () {
        
        List<Anao> a;
        
        try {
            InputStream fos = new FileInputStream(DWARF_FILE);
            ObjectInputStream ois = new ObjectInputStream(fos);
            a = (List<Anao>) ois.readObject();
            fos.close();
            ois.close();
            
        } catch (Exception e) {
            return new LinkedList<>();
        }
        
        return a;
    }
    
    // O nome é bem auto-explicativo
    public static void botaPraDentro(Object f, String fileName) {
        
        try {
            FileOutputStream fos = new FileOutputStream(fileName, false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f);
            oos.flush();
            oos.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
