/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.model;

import java.io.Serializable;

/**
 *
 * @author pietro
 */

// Essa é a festa que o anão vai
public class FestaDeAnao implements Serializable {

    // A festa tem nome, o cla que organizou,
    // e como é uma festa de anões, tem que ter cerveja
    private String nome, cla, tipoCerveja;

    // Tem tbm qnts anoes cabem nela
    // e qnts ingressos faltam
    private int capacidade, ing;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCla() {
        return cla;
    }

    public void setCla(String cla) {
        this.cla = cla;
    }

    public String getTipoCerveja() {
        return tipoCerveja;
    }

    public void setTipoCerveja(String tipoCerveja) {
        this.tipoCerveja = tipoCerveja;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getIng() {
        return ing;
    }

    public void setIng(int ing) {
        this.ing = ing;
    }
}
