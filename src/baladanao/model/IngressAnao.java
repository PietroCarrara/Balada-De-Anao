/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.model;

import java.io.Serializable;

/**
 *
 * @author pietro
 */
// Esse é o ingresso que o anão
// vai ter pra ir nas festas
public class IngressAnao implements Serializable {

    private String cod;

    public IngressAnao(FestaDeAnao f, Anao a) {

        // 3 primeiros chars
        String cla = f.getCla().substring(0, 3);

        // Numero do ingresso
        int cod = f.getIng();

        this.cod = cla + cod;
    }

    public String getCod() {
        return cod;
    }
}
