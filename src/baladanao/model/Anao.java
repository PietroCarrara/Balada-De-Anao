/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author pietro
 */

// Este é o nosso querido usuário
// anão, que só quer ir nas festas
// do forte anão
public class Anao implements Serializable {
    
    // Os anões tem nome, senha, clã, e claro, cor da barba
    private String nome, senha, cla, corBarba;
    
    // Eles também tem ingressos
    private LinkedList<IngressAnao> ingressos = new LinkedList<>();
    
    /*
        for (Entry<String, Integer> e : pontos.entrySet()) {
            
        }
    */
    
    // Um anão tem pontos com um clã
    // Esse map tem o nome do clã como key e os pontos
    // como value
    private Map<String, Integer> pontos = new HashMap<>();

    public Map<String, Integer> getPontos() {
        return pontos;
    }

    public boolean compraIngresso(FestaDeAnao f) {
        
        if (f.getIng() < 1)
            return false;
        
        IngressAnao i = new IngressAnao(f, this);
        ingressos.add(i);
        
        String cla = f.getCla();
        
        int pnts = 0;
        // O anão ganha o dobro de pontos
        // se for numa festa do seu clã
        if (cla.equals(this.cla)) {
            pnts = 2;
        } else {
            pnts = 1;
        }
        
        // Se ele ja tem pontos com esse cla,
        // some os pontos novos
        if (pontos.containsKey(cla)) {
            pnts += pontos.get(cla);
        }
        
        pontos.put(cla, pnts);   
        
        f.setIng(f.getIng() - 1);
        
        return true;
    }
    
    public String getNome() {
        
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
     public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCorBarba() {
        return corBarba;
    }

    public void setCorBarba(String corBarba) {
        this.corBarba = corBarba;
    }

    public String getCla() {
        return cla;
    }

    public void setCla(String cla) {
        this.cla = cla;
    }

    public LinkedList<IngressAnao> getIngressos() {
        return ingressos;
    }
}
