/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao;

import baladanao.model.Anao;
import baladanao.model.FestaDeAnao;
import baladanao.model.ForteAnao;
import baladanao.view.Dummy;
import java.io.IOException;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author pietro
 */

public class BaladAnao extends Application {

    public static List<Anao> anoes = ForteAnao.pegaOsAnao();
    public static List<FestaDeAnao> festas = ForteAnao.pegaAsFesta();
    
    public static Anao logado;
    
    public static Stage primaryStage;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Opções de debug
        if (anoes.size() < 1) {
            System.out.println("Lista de anoes vazia, enchendo linguiça...");
            
            Anao a = new Anao();
            
            a.setNome("Balin");
            a.setSenha("senha"); // Sem senha
            a.setCla("Sociedade Do Anel");
            a.setCorBarba("Branca");
            
            anoes.add(a);
        }
                
        // Opções de debug
        if (festas.size() < 1) {
            System.out.println("Lista de festas vazia, enchendo linguiça...");
            
            FestaDeAnao f = new FestaDeAnao();
            
            f.setNome("Inauguração Do Forte");
            f.setCla("Sociedade Do Anel");
            f.setTipoCerveja("Ale");
            f.setCapacidade(7);
            f.setIng(7);
            
            festas.add(f);
            
            anoes.get(0).compraIngresso(f);
        }
        
        ForteAnao.botaPraDentro(festas, ForteAnao.PARTY_FILE);
        ForteAnao.botaPraDentro(anoes, ForteAnao.DWARF_FILE);
        
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        primaryStage = stage;
        primaryStage.setTitle("Loja Maneira");
        primaryStage.setScene(getRootLayout());
        primaryStage.show();
    }
    
    private Scene getRootLayout() {
       Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Dummy.class.getResource("Login.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            
            scene = new Scene(rootLayout);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scene;
    }
    
}
