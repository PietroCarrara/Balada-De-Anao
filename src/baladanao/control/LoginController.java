/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.control;

import baladanao.BaladAnao;
import baladanao.model.Anao;
import baladanao.model.ForteAnao;
import baladanao.view.Dummy;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author pietro
 */
public class LoginController implements Initializable {

    @FXML
    TextField logUser;

    @FXML
    PasswordField logPass;

    @FXML
    TextField regUser;

    @FXML
    PasswordField regPass;

    @FXML
    TextField regCla;

    @FXML
    TextField regCorBarba;

    @FXML
    public void logar() {

        Anao an = null;

        for (Anao a : BaladAnao.anoes) {
            if (a.getNome().equals(logUser.getText()) && a.getSenha().equals(logPass.getText())) {
                an = a;
            }
        }

        if (an == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erro no login");
            alert.setHeaderText("Usuário/senha inválidos!");
            alert.setContentText("Talvez você ainda não possua uma conta, caro amigo anão!");

            alert.showAndWait();

            return;
        }

        BaladAnao.logado = an;
        goMainScreen();
    }

    @FXML
    public void registrar() {

        Anao an = null;

        for (Anao a : BaladAnao.anoes) {
            if (a.getNome().equals(logUser.getText()) && a.getSenha().equals(logPass.getText())) {
                an = a;
            }
        }

        if (an != null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erro no registro");
            alert.setHeaderText("Usuário já utilizado!");
            alert.setContentText("Tente outro nome, camarada anão!");

            alert.showAndWait();

            return;
        }

        an = new Anao();

        an.setNome(regUser.getText());
        an.setSenha(regPass.getText());
        an.setCla(regCla.getText());
        an.setCorBarba(regCorBarba.getText());

        BaladAnao.anoes.add(an);

        ForteAnao.botaPraDentro(BaladAnao.anoes, ForteAnao.DWARF_FILE);

        BaladAnao.logado = an;
        goMainScreen();
    }

    private void goMainScreen() {
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Dummy.class.getResource("Painel.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
            
            BaladAnao.primaryStage.setScene(scene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
}
