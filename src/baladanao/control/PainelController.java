/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.control;

import baladanao.BaladAnao;
import baladanao.model.IngressAnao;
import baladanao.view.Dummy;
import java.io.IOException;
import java.net.URL;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author pietro
 */
public class PainelController implements Initializable {

    @FXML
    private Label nome;

    @FXML
    private ListView<IngressAnao> ingressos;
    private ObservableList<IngressAnao> oIngressos;

    @FXML
    private ListView<Entry<String, Integer>> pontos;
    private ObservableList<Entry<String, Integer>> oPontos;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        oIngressos = ingressos.getItems();
        oPontos = pontos.getItems();

        ingressos.setCellFactory(param -> new ListCell<IngressAnao>() {
            @Override
            public void updateItem(IngressAnao i, boolean empty) {
                super.updateItem(i, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText("Código: " + i.getCod());
                }
                setGraphic(null);
            }
        });

        pontos.setCellFactory(param -> new ListCell<Entry<String, Integer>>() {
            @Override
            public void updateItem(Entry<String, Integer> i, boolean empty) {
                super.updateItem(i, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText("Pontos no clã " + i.getKey() + ": " + i.getValue());
                }
                setGraphic(null);
            }
        });

        oIngressos.addAll(BaladAnao.logado.getIngressos());
        oPontos.addAll(BaladAnao.logado.getPontos().entrySet());
        
        nome.setText("Bem-vindo, " + BaladAnao.logado.getNome());
    }
    
    @FXML
    private void goComprarIngresso() {
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Dummy.class.getResource("ComprarIngresso.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
            
            BaladAnao.primaryStage.setScene(scene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void goMarcarFesta() {
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Dummy.class.getResource("MarcarFesta.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
            
            BaladAnao.primaryStage.setScene(scene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
