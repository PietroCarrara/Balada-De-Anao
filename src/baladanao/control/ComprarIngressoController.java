/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baladanao.control;

import baladanao.BaladAnao;
import baladanao.model.FestaDeAnao;
import baladanao.model.ForteAnao;
import baladanao.view.Dummy;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author pietro
 */
public class ComprarIngressoController implements Initializable {

    @FXML
    private ListView<FestaDeAnao> festas;
    private ObservableList<FestaDeAnao> oFestas;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        oFestas = festas.getItems();

        festas.setCellFactory(param -> new ListCell<FestaDeAnao>() {
            @Override
            public void updateItem(FestaDeAnao f, boolean empty) {
                super.updateItem(f, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText("Festa '" + f.getNome() + "' do clã " + f.getCla() + ", com cerveja '" + f.getTipoCerveja() + "'." + f.getIng() + " ingressos restantes.");
                }
                setGraphic(null);
            }
        });

        oFestas.addAll(BaladAnao.festas);
    }

    @FXML
    public void comprar() {

        FestaDeAnao f = festas.getSelectionModel().getSelectedItem();

        // Se conseguiu comprar
        if (f != null && BaladAnao.logado.compraIngresso(f)) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Compra concluída");
            alert.setHeaderText("Ingresso adquirido com sucesso!");
            alert.setContentText("Você já pode ir e aproveitar a '" + f.getNome() + "'!");

            alert.showAndWait();

            goMainScreen();
        } else { // Senao
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erro na compra");
            alert.setHeaderText("Não foi possível adquirir o ingresso!");
            alert.setContentText("Não fique bravo, camarada! Ainda há muitas festas para ir!");

            alert.showAndWait();
        }

        ForteAnao.botaPraDentro(BaladAnao.festas, ForteAnao.PARTY_FILE);
        ForteAnao.botaPraDentro(BaladAnao.anoes, ForteAnao.DWARF_FILE);
    }

    private void goMainScreen() {
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Dummy.class.getResource("Painel.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);

            BaladAnao.primaryStage.setScene(scene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
