package baladanao.control;

import baladanao.BaladAnao;
import baladanao.model.FestaDeAnao;
import baladanao.model.ForteAnao;
import baladanao.view.Dummy;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author pietro
 */
public class MarcarFestaController implements Initializable {

    @FXML
    private TextField nome;
    @FXML
    private TextField cla;
    @FXML
    private TextField tipoCerveja;
    @FXML
    private TextField capacidade;

    public void marcar() {

        FestaDeAnao f = new FestaDeAnao();

        f.setNome(nome.getText());
        f.setCla(BaladAnao.logado.getCla());
        f.setTipoCerveja(tipoCerveja.getText());
        f.setCapacidade(Integer.parseInt(capacidade.getText()));
        f.setIng(f.getCapacidade());

        BaladAnao.festas.add(f);

        ForteAnao.botaPraDentro(BaladAnao.festas, ForteAnao.PARTY_FILE);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Festa Marcada");
        alert.setHeaderText("Festa Marcada com sucesso!");
        alert.setContentText("Você já pode ir e aproveitar a '" + f.getNome() + "'!");

        alert.showAndWait();

        ForteAnao.botaPraDentro(BaladAnao.festas, ForteAnao.PARTY_FILE);
        
        goMainScreen();
    }

    private void goMainScreen() {
        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Dummy.class.getResource("Painel.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);

            BaladAnao.primaryStage.setScene(scene);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cla.setText(BaladAnao.logado.getCla());
    }
}
